const tessel = require('tessel');
const pir = require('pir').use(tessel.port.A.pin[2]);
const PiCamera = require('pi-camera');

pir.on('ready', pir => {
  console.log('Ready and waiting...');
  pir.on('movement:start', time => {
	console.log(`Something moved! Time ${time}`);
	
	const myCamera = new PiCamera({
  		mode: 'video',
  		output: `${ __dirname }/video.h264`,
  		width: 1920,
  		height: 1080,
  		timeout: 5000, // Record for 5 seconds
  		nopreview: true,
	});
 
myCamera.record()
  .then((result) => {
    // Your video was captured
  })
  .catch((error) => {
     // Handle your error
  });



  });
  pir.on('movement:end', time => {
	console.log(`Everything stopped. Time ${time}`);
  });
});



