from gpiozero import MotionSensor
from picamera import PiCamera
from datetime import datetime

camera= PiCamera()
pir= MotionSensor(4)
while True:
    pir.wait_for_motion()
    print("Motion Detected!")
    try:
        camera.framerate =15
        #filename= datatime.now().strftime("%y-%m-%d_%H.%M.%S.h264")
        filename= '/home/pi/Desktop/video.h264' 
        camera.start_recording(filename)
        pir.wait_for_no_motion()
        print("Motion stopped!")
        camera.stop_recording()
        pass
    finally:    
        camera.close()
    break

